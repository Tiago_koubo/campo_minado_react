import React, {Component} from 'react';
import {StyleSheet, Text, View, Alert} from 'react-native';
import params from './src/params'
import MinasCampos from './src/components/MinasCampos'
import Cabecalho from './src/components/cabecalho'
import SelecionarDificuldade from './src/components/selecionarDificuldade'
import {
    criarMinasTabuleiro,
    clonarTabuleiro,
    abrirCampo,
    foiExplodido,
    ganhoujogo,
    mostrarMinas,
    marcarBandeira,
    bandeiraUsada
} from './src/logica'


export default class App extends Component{

    constructor(props) {
      super(props)
      this.state = this.criarEstado()
    }

    minasQuantidade = () => {
      const cols = params.getColumnsAmount()
      const rows = params.getRowsAmount()
      return Math.ceil(cols * rows * params.difficultLevel)
    }

    criarEstado = () => {
      const cols = params.getColumnsAmount()
      const rows = params.getRowsAmount()
      return { 
        tabuleiro: criarMinasTabuleiro(rows, cols, this.minasQuantidade()),
        ganhou: false,
        perdeu: false,
        mostrarSelecionarDificuldade: false
      }
    }

    onOpenField = ( row, column) => { 
      const tabuleiro = clonarTabuleiro(this.state.tabuleiro)
      abrirCampo(tabuleiro, row, column)
      const perdeu = foiExplodido(tabuleiro)
      const ganhou = ganhoujogo(tabuleiro)

      if (perdeu) {
        mostrarMinas(tabuleiro)
        Alert.alert('Perdeuuuuuu!')
      }
      
      if(ganhou) {
        Alert.alert(' Parabéns', 'Você Venceu!')
      }
      this.setState({ tabuleiro, perdeu, ganhou })
    }

    onSelectField = (row, column) => {
      const tabuleiro = clonarTabuleiro(this.state.tabuleiro)
      marcarBandeira(tabuleiro, row, column)
      const ganhou = ganhoujogo(tabuleiro)

      if(ganhou) {
        Alert.alert(' Parabéns', 'Você Venceu!')
      }

      this.setState({tabuleiro, ganhou})
   }

   onSobre = () => {
    Alert.alert('Criador: Tiago Koubo', 'e-mail:tiagokoubo@yahoo.com.br')
 }

    onLevelSelected = level => {
      params.difficultLevel = level
      this.setState(this.criarEstado())
    }
    

  render() {
    return (
      <View style={styles.container}>
        <SelecionarDificuldade isVisible={this.state.mostrarSelecionarDificuldade}
          onLevelSelected={this.onLevelSelected}
          onCancel={() => this.setState({mostrarSelecionarDificuldade: false })} />
        <Cabecalho flagsLeft={this.minasQuantidade() - bandeiraUsada(this.state.tabuleiro)}
          onNewGame={() => this.setState(this.criarEstado())}
          onFlagPress={() => this.setState({ mostrarSelecionarDificuldade: true })}
          onSobre={this.onSobre} />
        <View style={styles.tabuleiro}>
          <MinasCampos tabuleiro={this.state.tabuleiro} 
            onOpenField={this.onOpenField}
            onSelectField={this.onSelectField} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  tabuleiro: {
    alignItems: 'center',
    backgroundColor: '#AAA'
  }
});
