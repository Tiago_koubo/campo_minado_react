const criarTabuleiro = (rows, columns) => {
    return Array(rows).fill(0).map((_, row) => {
        return Array(columns).fill(0).map((_, column) => {
            return {
              row,
              column,
              aberto: false,
              bandeirado: false,
              minado: false,
              explodida: false,
              minasProxima: 0
            }
        })
    })
}

const espalharMinas = (tabuleiro, qtdMinas) => {
    const rows = tabuleiro.length
    const columns = tabuleiro[0].length
    let minasPlantadas = 0

    while (minasPlantadas < qtdMinas)  {
        const rowSel = parseInt(Math.random() * rows, 10)
        const columnSel = parseInt(Math.random() * columns, 10)
        
        if (!tabuleiro[rowSel][columnSel].minado) {
            tabuleiro[rowSel][columnSel].minado = true
            minasPlantadas ++
        }
    }
}

const criarMinasTabuleiro = (rows, columns, qtdMinas) => {
    const tabuleiro = criarTabuleiro(rows, columns)
    espalharMinas(tabuleiro, qtdMinas)
    return  tabuleiro
}

const clonarTabuleiro = tabuleiro => { 
    return tabuleiro.map(rows => {
        return rows.map(field => {
            return {...field }
        })
    })
}

const pegarVizinhos = (tabuleiro, row, column) => {
    const vizinhos = []
    const rows = [row - 1, row, row+1]
    const columns = [column - 1, column, column + 1]
    rows.forEach(r => { 
        columns.forEach(c => {
            const diferent = r !== row  || c !== column
            const validRow = r >= 0 && r < tabuleiro.length
            const validColumn = c >= 0 && c < tabuleiro[0].length
            if (diferent && validRow && validColumn) {
                vizinhos.push(tabuleiro[r][c])
            }
        })
    })
    return vizinhos
}

const vizinhancaSegura = (tabuleiro, row, column) => {
    const safes = (result, vizinhos) => result && !vizinhos.minado
    return pegarVizinhos(tabuleiro, row, column).reduce(safes, true)
}

const abrirCampo = (tabuleiro, row, column) => {
    const campo = tabuleiro[row][column]
    if (!campo.aberto) {
        campo.aberto = true
        if(campo.minado) {
            campo.explodida = true
        } else if (vizinhancaSegura(tabuleiro, row, column)) {
            pegarVizinhos(tabuleiro, row, column)
                .forEach(n=> abrirCampo(tabuleiro, n.row, n.column))
        } else {
            const vizinhos = pegarVizinhos(tabuleiro, row, column)
            campo.minasProxima = vizinhos.filter(n=>n.minado).length
        }
    }
}

const campos = tabuleiro => [].concat(...tabuleiro)
const foiExplodido = tabuleiro => campos(tabuleiro)
        .filter(campo=>campo.explodida).length > 0
const pendente = campo => (campo.minado && !campo.bandeirado)
        || (!campo.minado && !campo.aberto)
const ganhoujogo = tabuleiro => campos(tabuleiro).filter(pendente).length === 0
const mostrarMinas = tabuleiro => campos(tabuleiro).filter(campo=> campo.minado)
        .forEach(campo => campo.aberto=true)

const marcarBandeira = (tabuleiro, row, column) => {
    const campo = tabuleiro[row][column]
    campo.bandeirado = !campo.bandeirado
}

const bandeiraUsada = tabuleiro => campos(tabuleiro)
        .filter(campo => campo.bandeirado).length


export {
    criarMinasTabuleiro,
    clonarTabuleiro,
    abrirCampo,
    foiExplodido,
    ganhoujogo,
    mostrarMinas,
    marcarBandeira,
    bandeiraUsada      
}