import React from 'react'
import {View, StyleSheet, Text, TouchableWithoutFeedback } from 'react-native'
import params from '../params'
import Mina from './mina'
import Bandeira from './bandeira'

export default props => {
    const { minado, aberto, minasProxima, explodida, bandeirado } = props

    const styleField = [styles.field]
    if (aberto) styleField.push(styles.aberto)
    if (explodida) styleField.push(styles.explodida)
    if (bandeirado) styleField.push(styles.bandeirado)
    if (!aberto && !explodida) styleField.push(styles.regular)

    let color = null
    if (minasProxima > 0) {
        if (minasProxima == 1) color = '#2A28D7'
        if (minasProxima == 2) color = '#006400'
        if (minasProxima > 2 && minasProxima < 6) color = '#F9060A'
        if (minasProxima >= 6) color = '#C71585'
    }

    return (
         <TouchableWithoutFeedback onPress={props.onOpen}
             onLongPress={props.onSelect}>
            <View style={styleField}>
                 {!minado && aberto && minasProxima > 0 ?
                    <Text style={[styles.label, { color: color }]}>
                        {minasProxima}</Text> : false}
                 {minado && aberto ? <Mina /> : false}      
                 {bandeirado && !aberto ? <Bandeira /> : false}
             </View>
         </TouchableWithoutFeedback>
     )
}

const styles = StyleSheet.create({
    field: {
        height: params.blockSize,
        width: params.blockSize,
        borderWidth: params.borderSize,
    },
    regular: {
        backgroundColor: '#999',
        borderLeftColor: '#CCC',
        borderTopColor: '#CCC',
        borderRightColor: '#333',
        borderBottomColor: '#333',
    },
    aberto: {
        backgroundColor: '#999',
        borderColor: '#777',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontWeight: 'bold',
        fontSize: params.fontSize,
    },
    explodida: {
        backgroundColor: 'red',
        borderColor: 'red',
    }
})  